package com.stefan.hogwarts.providers

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.stefan.hogwarts.MyRequestQueue
import com.stefan.hogwarts.models.*

import java.util.*

class PotterDataProvider {

    var BASE_URL: String = "https://www.potterapi.com/v1"
    var API_KEY: String = "$2a$10$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y"
    fun getQueryUrl(query: String): String {
        return "$BASE_URL/$query&key=$API_KEY"
    }
    fun getHouses(context: Context,responseHandler : (result: HousesResult) -> Unit?){
        Log.e("URL",getQueryUrl("houses"))
        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET, getQueryUrl("houses?"), null,
            Response.Listener { response ->
//                Log.e("Houses",response.toString())
                val gson = Gson()
                val arrayHouseType = object : TypeToken<Array<House>>() {}.type

                val houses: Array<House> = gson.fromJson(response.toString(), arrayHouseType)

                responseHandler.invoke(HousesResult(houses))
            },
            Response.ErrorListener { error ->
                // TODO: Handle error
            }
        )

        MyRequestQueue.getInstance(context).addToRequestQueue(jsonArrayRequest)

    }

    fun getCharacters(house: String, context: Context, responseHandler : (result: MemberResult) -> Unit?){
        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET, getQueryUrl("characters?house=$house"), null,
            Response.Listener { response ->
                //                Log.e("Houses",response.toString())
                val gson = Gson()
                val arrayMemberType = object : TypeToken<Array<Member>>() {}.type

                val members: Array<Member> = gson.fromJson(response.toString(), arrayMemberType)

                responseHandler.invoke(MemberResult(members))
            },
            Response.ErrorListener { error ->
                // TODO: Handle error
            }
        )

        MyRequestQueue.getInstance(context).addToRequestQueue(jsonArrayRequest)
    }


    fun getSpells(context: Context, responseHandler : (result: SpellResult) -> Unit?){
        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET, getQueryUrl("spells?"), null,
            Response.Listener { response ->
                //                Log.e("Houses",response.toString())
                val gson = Gson()
                val arraySpellType = object : TypeToken<Array<Spell>>() {}.type

                val spells: Array<Spell> = gson.fromJson(response.toString(), arraySpellType)

                responseHandler.invoke(SpellResult(spells))
            },
            Response.ErrorListener { error ->
                // TODO: Handle error
            }
        )

        MyRequestQueue.getInstance(context).addToRequestQueue(jsonArrayRequest)
    }
}