package com.stefan.hogwarts.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.stefan.hogwarts.R
import com.stefan.hogwarts.interfaces.OnItemClickListener
import com.stefan.hogwarts.models.House
import com.stefan.hogwarts.models.Spell

class SpellsAdapter (private var mSpells: List<Spell>): RecyclerView.Adapter<SpellsAdapter.ViewHolder>()  {

    private var context: Context? = null
    fun setSpells(spells: List<Spell>){
        mSpells = spells
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val contactView = inflater.inflate(R.layout.spell_row, parent, false)

        return ViewHolder(contactView)

    }

    override fun getItemCount(): Int {
        return mSpells.size
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        val spell: Spell = mSpells.get(position)

        when {
            spell.type == "Spell" -> {
                holder.spellTypeImageView.setImageResource(R.drawable.spell)
            }
            spell.type == "Curse" -> {
                holder.spellTypeImageView.setImageResource(R.drawable.curse)
            }
            spell.type == "Charm" -> {
                holder.spellTypeImageView.setImageResource(R.drawable.charm)
            }
            spell.type == "Enchantment" -> {
                holder.spellTypeImageView.setImageResource(R.drawable.enchantment)
            }
        }

        holder.spellNameTextView.text = spell.spell
        holder.spellEffectTextView.text = spell.effect
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val spellNameTextView = itemView.findViewById<TextView>(R.id.spellNameTV)
        val spellEffectTextView = itemView.findViewById<TextView>(R.id.spellEffectTV)
        val spellTypeImageView = itemView.findViewById<ImageView>(R.id.spellTypeIV)
    }
}