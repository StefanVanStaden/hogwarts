package com.stefan.hogwarts.adapters

import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stefan.hogwarts.R
import com.stefan.hogwarts.interfaces.OnItemClickListener
import com.stefan.hogwarts.models.House
import com.stefan.hogwarts.models.Member

class MemberAdapter (private var mMembers: List<Member>,val itemClickListener: OnItemClickListener): RecyclerView.Adapter<MemberAdapter.ViewHolder>()  {


    fun setMembers(members: List<Member>){
        mMembers = members
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val contactView = inflater.inflate(R.layout.members_row, parent, false)

        return ViewHolder(contactView)

    }

    override fun getItemCount(): Int {
        return mMembers.size
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        val member: Member = mMembers.get(position)

        if(member.deathEater!!){
            holder.deathEaterIV.visibility = View.VISIBLE
        }else{
            holder.deathEaterIV.visibility = View.GONE
        }
        if(member.ministryOfMagic!!){
            holder.ministryOfMagicIV.visibility = View.VISIBLE
        }else{
            holder.ministryOfMagicIV.visibility = View.GONE
        }
        if(member.orderOfThePhoenix!!){
            holder.orderOfPheonixIV.visibility = View.VISIBLE
        }else{
            holder.orderOfPheonixIV.visibility = View.GONE
        }
        if(member.dumbledoresArmy!!){
            holder.dumbledoresArmyIV.visibility = View.VISIBLE
        }else{
            holder.dumbledoresArmyIV.visibility = View.GONE
        }

        holder.memberNameTextView.text = member.name
        holder.memberRoleTextView.text = member.role
        holder.bind(member,itemClickListener)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val memberNameTextView = itemView.findViewById<TextView>(R.id.memberNameTV)
        val memberRoleTextView = itemView.findViewById<TextView>(R.id.memberRoleTV)
        val ministryOfMagicIV = itemView.findViewById<ImageView>(R.id.ministryOfMagicIV)
        val orderOfPheonixIV= itemView.findViewById<ImageView>(R.id.orderOfPheonixIV)
        val dumbledoresArmyIV = itemView.findViewById<ImageView>(R.id.dumbledoresArmyIV)
        val deathEaterIV = itemView.findViewById<ImageView>(R.id.deathEaterIV)

        fun bind(member: Member,clickListener: OnItemClickListener){

            itemView.setOnClickListener{
                clickListener.onItemClicked(member)
            }
        }
    }
}