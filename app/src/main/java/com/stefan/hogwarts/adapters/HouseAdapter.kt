package com.stefan.hogwarts.adapters

import android.app.Application
import android.content.Context
import android.graphics.Color
import android.provider.Settings.Global.getString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.stefan.hogwarts.R
import com.stefan.hogwarts.interfaces.OnItemClickListener
import com.stefan.hogwarts.models.House

class HouseAdapter (private var mHouses: List<House>,val itemClickListener: OnItemClickListener): RecyclerView.Adapter<HouseAdapter.ViewHolder>()  {

    private var context: Context? = null
    fun setHouses(houses: List<House>){
        mHouses = houses
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val contactView = inflater.inflate(R.layout.house_row, parent, false)

        return ViewHolder(contactView)

    }

    override fun getItemCount(): Int {
        return mHouses.size
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        val house: House = mHouses.get(position)

        when {
            house.name == "Gryffindor" -> {
                holder.houseImageView.setImageResource(R.drawable.gryffindor_clear)
                holder.headOfHouseNametextView.setTextColor(getColor(this.context!!,R.color.gold))
                holder.houseNameTextView.setTextColor(getColor(this.context!!,R.color.gold))
            }
            house.name == "Slytherin" -> {
                holder.houseImageView.setImageResource(R.drawable.slytherin_clear)
                holder.headOfHouseNametextView.setTextColor(getColor(this.context!!,R.color.silver))
                holder.houseNameTextView.setTextColor(getColor(this.context!!,R.color.silver))
            }
            house.name == "Hufflepuff" -> {
                holder.houseImageView.setImageResource(R.drawable.hufflepuff_clear)
                holder.headOfHouseNametextView.setTextColor(getColor(this.context!!,R.color.black))
                holder.houseNameTextView.setTextColor(getColor(this.context!!,R.color.black))
            }
            house.name == "Ravenclaw" -> {
                holder.houseImageView.setImageResource(R.drawable.ravenclaw_clear)
                holder.headOfHouseNametextView.setTextColor(getColor(this.context!!,R.color.bronze))
                holder.houseNameTextView.setTextColor(getColor(this.context!!,R.color.bronze))
            }
        }

        holder.headOfHouseNametextView.text = house.headOfHouse
        holder.houseNameTextView.text = house.name

        holder.bind(mHouses.get(position),itemClickListener)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val houseNameTextView = itemView.findViewById<TextView>(R.id.houseNameTV)
        val headOfHouseNametextView = itemView.findViewById<TextView>(R.id.headOfHouseTV)
        val houseImageView = itemView.findViewById<ImageView>(R.id.houseIV)
        fun bind(house: House,clickListener: OnItemClickListener){

            itemView.setOnClickListener{
                clickListener.onItemClicked(house)
            }
        }
    }
}