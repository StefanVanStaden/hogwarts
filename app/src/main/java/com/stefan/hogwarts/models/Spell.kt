package com.stefan.hogwarts.models

class Spell {
    val id: String? = null
    val spell: String? = null
    val type: String? = null
    val effect: String? = null
    val v: Long? = null
}