package com.stefan.hogwarts.models

import java.io.Serializable

class Member: Serializable{

    val id: String? = null
    val name: String? = null
    val role: String? = null
    val house: String? = null
    val school: String? = null
    val v: Long? = null
    val ministryOfMagic: Boolean? = null
    val orderOfThePhoenix: Boolean? = null
    val dumbledoresArmy: Boolean? = null
    val deathEater: Boolean? = null
    val bloodStatus: String? = null
    val species: String? = null
}