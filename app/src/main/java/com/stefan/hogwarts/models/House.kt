package com.stefan.hogwarts.models

import java.io.Serializable

class House: Serializable {

    val id: String? = null
    val name: String? = null
    val mascot: String? = null
    val headOfHouse: String? = null
    val houseGhost: String? = null
    val founder: String? = null
    val v: Long? = null
    val school: String? = null
    val members: List<String>? = null
    val values: List<String>? = null
    val colors: List<String>? = null
}