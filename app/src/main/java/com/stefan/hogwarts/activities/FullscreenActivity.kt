package com.stefan.hogwarts.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stefan.hogwarts.R
import com.stefan.hogwarts.adapters.HouseAdapter
import com.stefan.hogwarts.interfaces.OnItemClickListener
import com.stefan.hogwarts.models.House
import com.stefan.hogwarts.models.Member
import com.stefan.hogwarts.providers.PotterDataProvider
import kotlinx.android.synthetic.main.activity_fullscreen.*
import kotlinx.android.synthetic.main.activity_fullscreen.recyclerView as recyclerView

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FullscreenActivity : AppCompatActivity(), OnItemClickListener{


    private val potterDataProvider: PotterDataProvider = PotterDataProvider()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private val houseAdapter: HouseAdapter = HouseAdapter(ArrayList<House>(),this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fullscreen)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = houseAdapter

        potterDataProvider.getHouses(this) { HousesResult ->


//            val house: House = HousesResult.houses[0]

            runOnUiThread {
                houseAdapter.setHouses(HousesResult.houses.asList())
            }

        }

    }

    override fun onItemClicked(member: Member) {

    }

    override fun onItemClicked(house: House) {
        val intent = Intent(this, HouseActivity::class.java)
        intent.putExtra("house", house)
        startActivity(intent)
    }

    fun gotoSpells(view: View) {
        val intent = Intent(this, SpellsActivity::class.java)
        startActivity(intent)
    }


}

