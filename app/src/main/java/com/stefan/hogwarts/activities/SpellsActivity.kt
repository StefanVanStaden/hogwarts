package com.stefan.hogwarts.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.stefan.hogwarts.R
import com.stefan.hogwarts.adapters.MemberAdapter
import com.stefan.hogwarts.adapters.SpellsAdapter
import com.stefan.hogwarts.models.Member
import com.stefan.hogwarts.models.Spell
import com.stefan.hogwarts.providers.PotterDataProvider
import kotlinx.android.synthetic.main.activity_house.*
import kotlinx.android.synthetic.main.activity_spells.*

class SpellsActivity : AppCompatActivity() {
    private val potterDataProvider: PotterDataProvider = PotterDataProvider()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private val spellAdapter: SpellsAdapter = SpellsAdapter(ArrayList<Spell>())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spells)

        linearLayoutManager = LinearLayoutManager(this)
        spellsRV.layoutManager = linearLayoutManager
        spellsRV.adapter = spellAdapter

        potterDataProvider.getSpells(this) { SpellResult ->
            runOnUiThread {
                spellAdapter.setSpells(SpellResult.spells.asList())
            }
        }

    }
}
