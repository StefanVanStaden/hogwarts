package com.stefan.hogwarts.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.stefan.hogwarts.R
import com.stefan.hogwarts.adapters.MemberAdapter
import com.stefan.hogwarts.interfaces.OnItemClickListener
import com.stefan.hogwarts.models.House
import com.stefan.hogwarts.models.Member
import com.stefan.hogwarts.providers.PotterDataProvider
import kotlinx.android.synthetic.main.activity_house.*

class HouseActivity : AppCompatActivity(), OnItemClickListener {


    private val potterDataProvider: PotterDataProvider = PotterDataProvider()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private val memberAdapter: MemberAdapter = MemberAdapter(ArrayList<Member>(),this)
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_house)
        val house = intent.getSerializableExtra("house") as? House

        if (house != null) {
            title = house.name

            houseFounderTV.text = "Founder: ${house.founder}"
            houseGhostTV.text = "Ghost:  ${house.houseGhost}"
            houseMascotTV.text = "Mascot:  ${house.mascot}"
            var houseValues = "Values: "
            house.values?.forEach {
                houseValues += " $it,"
            }

            houseValuesTV.text = houseValues.trimEnd(',')

            when {
                house.name == "Gryffindor" -> {

                    houseLogoIV.setImageResource(R.drawable.gryffindor_clear)
                    houseTitleTv.text = house.name
                    houseTitleTv.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.gold
                        )
                    )
                    updateStatusBarColor( R.color.scarlet,false)
                    houseValuesLL.setBackgroundColor(ContextCompat.getColor(
                        applicationContext,
                        R.color.scarlet
                    ))
                    houseFounderTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.gold
                        )
                    )
                    houseGhostTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.gold
                        )
                    )
                    houseMascotTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.gold
                        )
                    )
                    houseValuesTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.gold
                        )
                    )
                }
                house.name == "Slytherin" -> {
                    houseLogoIV.setImageResource(R.drawable.slytherin_clear)
                    houseTitleTv.text = house.name
                    houseTitleTv.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.silver
                        )
                    )
                    updateStatusBarColor( R.color.green,false)
                    houseValuesLL.setBackgroundColor(ContextCompat.getColor(
                        applicationContext,
                        R.color.green
                    ))
                    houseFounderTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.silver
                        )
                    )
                    houseGhostTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.silver
                        )
                    )
                    houseMascotTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.silver
                        )
                    )
                    houseValuesTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.silver
                        )
                    )
                }
                house.name == "Hufflepuff" -> {
                    houseLogoIV.setImageResource(R.drawable.hufflepuff_clear)
                    houseTitleTv.text = house.name
                    houseTitleTv.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.black
                        )
                    )
                    updateStatusBarColor( R.color.yellow,true)
                    houseValuesLL.setBackgroundColor(ContextCompat.getColor(
                        applicationContext,
                        R.color.yellow
                    ))
                    houseFounderTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.black
                        )
                    )
                    houseGhostTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.black
                        )
                    )
                    houseMascotTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.black
                        )
                    )
                    houseValuesTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.black
                        )
                    )
                }
                house.name == "Ravenclaw" -> {
                    houseLogoIV.setImageResource(R.drawable.ravenclaw_clear)
                    houseTitleTv.text = house.name
                    houseTitleTv.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.bronze
                        )
                    )
                    updateStatusBarColor( R.color.blue,true)
                    houseValuesLL.setBackgroundColor(ContextCompat.getColor(
                        applicationContext,
                        R.color.blue
                    ))
                    houseFounderTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.bronze
                        )
                    )
                    houseGhostTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.bronze
                        )
                    )
                    houseMascotTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.bronze
                        )
                    )
                    houseValuesTV.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.bronze
                        )
                    )
                }
            }
        }


        linearLayoutManager = LinearLayoutManager(this)
        membersRV.layoutManager = linearLayoutManager
        membersRV.adapter = memberAdapter

        house?.name?.let {
            Log.e("MEMBERS","Loading members...")
            potterDataProvider.getCharacters(it,this) { MemberResult ->
                runOnUiThread {
                    memberAdapter.setMembers(MemberResult.members.asList())
                }
            }
        }

    }

    override fun onItemClicked(member: Member) {
        val intent = Intent(this, MemberDetailActivity::class.java)
        intent.putExtra("member", member)
        startActivity(intent)
    }

    override fun onItemClicked(house: House) {

    }

    fun updateStatusBarColor(@ColorRes colorId: Int, isStatusBarFontDark: Boolean = true) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, colorId)
            window.navigationBarColor = ContextCompat.getColor(this, colorId)
            setSystemBarTheme(isStatusBarFontDark)
        }
    }

    /** Changes the System Bar Theme.  */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private fun setSystemBarTheme(isStatusBarFontDark: Boolean) {
        // Fetch the current flags.
        val lFlags = window.decorView.systemUiVisibility
        // Update the SystemUiVisibility depending on whether we want a Light or Dark theme.
        window.decorView.systemUiVisibility = if (isStatusBarFontDark) lFlags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() else lFlags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }
}
