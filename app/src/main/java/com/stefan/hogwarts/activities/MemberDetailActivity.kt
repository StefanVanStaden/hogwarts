package com.stefan.hogwarts.activities

import android.graphics.ColorFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.stefan.hogwarts.R
import com.stefan.hogwarts.models.House
import com.stefan.hogwarts.models.Member
import kotlinx.android.synthetic.main.activity_member_detail.*
import kotlinx.android.synthetic.main.members_row.*

class MemberDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_detail)
        val member = intent.getSerializableExtra("member") as? Member

        if (member != null) {
            if(member.ministryOfMagic!! ){
                ministryOfMagicLL.visibility = View.VISIBLE
            }else{
                ministryOfMagicLL.visibility = View.GONE
            }
            if(member.orderOfThePhoenix!! ){
                orderOfPheonixLL.visibility = View.VISIBLE
            }else{
                orderOfPheonixLL.visibility = View.GONE
            }
            if(member.deathEater!! ){
                deathEaterLL.visibility = View.VISIBLE
            }else{
                deathEaterLL.visibility = View.GONE
            }
            if(member.dumbledoresArmy!! ){
                dumbledoresArmyLL.visibility = View.VISIBLE
            }else{
                dumbledoresArmyLL.visibility = View.GONE
            }
            nameTV.text = "Name: ${member.name}"
            roleTV.text = "Role: ${member.role}"
            houseTV.text = "House: ${member.house}"
            schoolTV.text = "School: ${member.school}"
            speciesTV.text = "Species: ${member.species}"
            bloodTV.text = "Blood status: ${member.bloodStatus}"

        }



    }
}
