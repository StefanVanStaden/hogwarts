package com.stefan.hogwarts.interfaces

import com.stefan.hogwarts.models.House
import com.stefan.hogwarts.models.Member

interface OnItemClickListener {
    fun onItemClicked(member: Member)

    fun onItemClicked(house: House)
}